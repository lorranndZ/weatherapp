public class Coord
{
    private static double lon;

    private static double lat;

    public double getLon ()
    {
        return lon;
    }

    public void setLon (double lon)
    {
        Coord.lon = lon;
    }

    public double getLat ()
    {
        return lat;
    }

    public void setLat (double lat)
    {
        Coord.lat = lat;
    }

    @Override
    public String toString()
    {
        return "[lon = "+lon+", lat = "+lat+"]";
    }
}
			