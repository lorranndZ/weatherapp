import java.net.*;
import java.io.*;
import java.io.IOException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class JasonTojava extends Application {
	
	static Main infoWeather = new Main();
	static TestTe mesajeDiferite  = new TestTe();
	static Weather iconWeather = new Weather();
	static IconMap iconHmap = new IconMap();
	
	static TextField inputCity;
	static Label showCityName, showWeatherImg, showCityTemp, showCItyHumidity,
						 showCityPressure, warningMessage;
	
	@Override
	public void start (Stage myWindow) throws IOException {
		
		// my application title
		Label programTitle = new Label("B.L Weather App");
		programTitle.setFont(Font.font("Courier", FontWeight.SEMI_BOLD, 20));
		programTitle.setTextFill(Color.web("6600CC"));
		inputCity = new TextField("Introduceti orasul dorit");
		inputCity.setOnMouseClicked(e ->inputCity.clear());
		inputCity.setPrefWidth(140);
		
		warningMessage = new Label();
		
		Button submitButton = new Button(" Submit ");
		submitButton.setOnAction(e-> { 	jsonBinding(urlContent(inputCity.getText()));
															 	if ( mesajeDiferite.getCod() == 200) {
															 		printOk();
															 	} else if (mesajeDiferite.getCod() == 404) {
															 		printError();
															 	} else {
															 		printNoConnection();
															 	}
													});
		submitButton.setPrefWidth(70);
		Button clearButton = new Button(" Clear ");
		clearButton.setOnAction(e -> clearWeatherInfo());
		clearButton.setPrefWidth(70);
	
			
		HBox submitClear = new HBox(15);
		submitClear.getChildren().addAll(submitButton, clearButton);
		VBox inputVbox = new VBox(15);
		inputVbox.getChildren().addAll(programTitle, inputCity, warningMessage, submitClear);
		Label outputOras = new Label("City:");
		Label outputTemperatura = new Label("Temprature:");
		Label outputUmiditate = new Label("Humidity:");
		Label outputPresiune = new Label ("Pressure:");
		showCityName = new Label();
		showCityTemp = new Label();
		showCItyHumidity  = new Label();
		showCityPressure = new Label();
		showWeatherImg = new Label();
		GridPane gridpane = new GridPane();
		gridpane.setPadding(new Insets(10));
		gridpane.setHgap(5);
		gridpane.setVgap(10);
		gridpane.getColumnConstraints().add(new ColumnConstraints(80));
		gridpane.getColumnConstraints().add(new ColumnConstraints(80));
		gridpane.getColumnConstraints().add(new ColumnConstraints(80));
		GridPane.setConstraints(outputOras, 0, 0);
		GridPane.setConstraints(outputTemperatura, 0, 1);
		GridPane.setConstraints(outputUmiditate, 0, 2);
		GridPane.setConstraints(outputPresiune, 0, 3);
		GridPane.setConstraints(showCityName, 1, 0);
		GridPane.setConstraints(showCityTemp, 1, 1);
		GridPane.setConstraints(showCItyHumidity, 1, 2);
		GridPane.setConstraints(showCityPressure, 1, 3);
		GridPane.setConstraints(showWeatherImg, 2, 0);
		gridpane.getChildren().addAll(outputOras, outputTemperatura, outputUmiditate, outputPresiune, 
														   showCityName, showCityTemp, showCItyHumidity, showCityPressure,
														   showWeatherImg);
		HBox hboxOne = new HBox(50);
		hboxOne.setPadding(new Insets(20,20,20,20));
		hboxOne.getChildren().addAll(inputVbox, gridpane);
		
		Scene sceneOne = new Scene(hboxOne, 500, 300);
		
		myWindow.setTitle("B.L Weather App");
		myWindow.centerOnScreen();
		myWindow.setResizable(false);
		myWindow.setScene(sceneOne);
		myWindow.show(); 
	}
	
	// citeste continut din url
	public static String urlContent(String theUrl) {
		String theUrlwSpace = theUrl.replaceAll("\\s","");
		StringBuilder content = new StringBuilder();
		String urlGoogleWeather = "http://api.openweathermap.org/data/2.5/weather?q=" + theUrlwSpace;

		try {
			URL url = new URL(urlGoogleWeather);
			URLConnection urlConnection = url.openConnection();

			BufferedReader bufferdReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
			String line;

			while ((line = bufferdReader.readLine()) != null) {
				content.append(line + "\n");
			}
			bufferdReader.close();
			
		} catch (MalformedURLException e) {
			warningMessage.setText("Nu aveti Conexiune");		
		} catch (IOException e) {
			warningMessage.setText("Nu aveti Conexiune");
		}
		return content.toString();
	}
	
	// mapeaza informatia primita la clasele java
	public static void jsonBinding(String result) {
		
		ObjectMapper mapper = new ObjectMapper();
		try {
			@SuppressWarnings("unused")
			TestTe test = mapper.readValue(result, TestTe.class);
			mapper.enable(SerializationConfig.Feature.INDENT_OUTPUT);
	
		} catch (JsonParseException e) {
			warningMessage.setText("eroare");
		} catch (JsonMappingException e) {
			warningMessage.setText("eroare");
		} catch (IOException e) {
		// mesajAvertizare.setText("Nu aveti Conexiune");
		}
	}
	public static void printOk() {
		showCityName.setText(inputCity.getText());
 		showCityTemp.setText(Integer.toString(infoWeather.getTemp())); 
	 	showCItyHumidity.setText(Integer.toString((int) infoWeather.getHumidity()));
	 	showCityPressure.setText(Integer.toString(infoWeather.getPressure()));
 	    warningMessage.setText("OK !!!");
 	    warningMessage.setTextFill(Color.web("#008000"));
 	    warningMessage.setFont(Font.font (null, FontWeight.BOLD, 14));				
 	    Image img = new Image(IconMap.hashMapOne.get(iconWeather.getIcon()));
 	    showWeatherImg.setGraphic(new ImageView(img));
 	    showWeatherImg.setVisible(true);
	}
	
	public static void printError() {
		warningMessage.setText(mesajeDiferite.getMessage());
 		warningMessage.setTextFill(Color.web("#FF0000"));
 	    warningMessage.setFont(Font.font (null, FontWeight.BOLD, 14));
		showCityName.setText("error "); // in loc sa stergi labelul il suprascrii cu spatiu
		showCityName.setTextFill(Color.web("#FF0000"));
		showCityTemp.setText("error "); 
		showCityTemp.setTextFill(Color.web("#FF0000"));
		showCItyHumidity.setText("error ");
		showCItyHumidity.setTextFill(Color.web("#FF0000"));
		showCityPressure.setText("error ");
		showCityPressure.setTextFill(Color.web("#FF0000"));
	}
	
	public static void printNoConnection() {
		warningMessage.setText("No Connection");
 		warningMessage.setTextFill(Color.web("#FF0000"));
 		warningMessage.setFont(Font.font (null, FontWeight.BOLD, 14));
	}
	
	public static void clearWeatherInfo() {
		inputCity.clear();
		inputCity.setText(" ");
		showCityName.setText(" "); // don't delete the label overwrite it with space
		showCityTemp.setText(" "); 
		showCItyHumidity.setText(" ");
		showCityPressure.setText(" ");
		warningMessage.setText(" ");
		showWeatherImg.setVisible(false);
	}
	public static void main(String[] args) {
		launch(args);
	}
}
