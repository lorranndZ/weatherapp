                 
public class Weather
{
    private static int id;

    private static String icon;

    private static String description;

    private static String main;

    public int getId ()
    {
        return id;
    }

    public void setId (int id)
    {
        Weather.id = id;
    }

    public String getIcon ()
    {
        return icon;
    }

    public void setIcon (String icon)
    {
        Weather.icon = icon;
    }

    public String getDescription ()
    {
        return description;
    }

    public void setDescription (String description)
    {
        Weather.description = description;
    }

    public String getMain ()
    {
        return main;
    }

    public void setMain (String main)
    {
        Weather.main = main;
    }

    @Override
    public String toString()
    {
        return "[id = "+id+", icon = "+icon+", description = "+description+", main = "+main+"]";
    }
}