import java.util.Map;
import java.util.HashMap;

public class IconMap {
	static // Create Map
	Map<String , String>  hashMapOne = new HashMap<String , String>();
	
	// Add keys and values
	static {
	 hashMapOne.put("01d", "/Icons/01d.png");
	 hashMapOne.put("02d" , "/Icons/02d.png");
	 hashMapOne.put("03d", "/Icons/03d.png");
	 hashMapOne.put("04d", "/Icons/04d.png");
	 hashMapOne.put("09d", "/Icons/09d.png");
	 hashMapOne.put("10d", "/Icons/10d.png");
	 hashMapOne.put("11d", "/Icons/11d.png");
	 hashMapOne.put("13d", "/Icons/13d.png");
	 hashMapOne.put("50d", "/Icons/50d.png");
	 hashMapOne.put("01n", "/Icons/01n.png");
	 hashMapOne.put("02n", "/Icons/02n.png");
	 hashMapOne.put("03n", "/Icons/03n.png");
	 hashMapOne.put("04n", "/Icons/04n.png");
	 hashMapOne.put("09n", "/Icons/09n.png");
	 hashMapOne.put("10n", "/Icons/10n.png");
	 hashMapOne.put("11n", "/Icons/11n.png");
	 hashMapOne.put("13n", "/Icons/13n.png");
	 hashMapOne.put("50n", "/Icons/50n.png");
	}
}
