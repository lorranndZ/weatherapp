
public class TestTe
{
    private  static int id;

    private static long dt;

    private Clouds clouds;

    private Coord coord;

    private Wind wind;
    
    private Rain rain;
    
    private Snow snow;
    
    private static int visibility;

    private static int cod;
    
    private  static String message;

    private Sys sys;

    private static String name;

    private static String base;

    private Weather[] weather;

    private Main main;
    
    

    public int getId ()
    {
        return id;
    }

    public void setId (int id)
    {
        TestTe.id = id;
    }

    public long getDt ()
    {
        return dt;
    }

    public void setDt (long dt)
    {
        TestTe.dt = dt;
    }

    public Clouds getClouds ()
    {
        return clouds;
    }

    public void setClouds (Clouds clouds)
    {
        this.clouds = clouds;
    }

    public Coord getCoord ()
    {
        return coord;
    }

    public void setCoord (Coord coord)
    {
        this.coord = coord;
    }

    public Wind getWind ()
    {
        return wind;
    }
    
    public int getvisibility(){
    	return visibility;
    }
    
    public void setVisibility(int visibility){
    	TestTe.visibility = visibility;
    }

    public void setWind (Wind wind)
    {
        this.wind = wind;
    }

    public int getCod ()
    {
        return cod;
    }

    public void setCod (int cod)
    {
        TestTe.cod = cod;
    }

    public Sys getSys ()
    {
        return sys;
    }

    public void setSys (Sys sys)
    {
        this.sys = sys;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        TestTe.name = name;
    }

    public String getBase ()
    {
        return base;
    }

    public void setBase (String base)
    {
        TestTe.base = base;
    }

    public Weather[] getWeather ()
    {
        return weather;
    }

    public void setWeather (Weather[] weather)
    {
        this.weather = weather;
    }
    
    public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		TestTe.message = message;
	}

    public Main getMain ()
    {
        return main;
    }

    public void setMain (Main main)
    {
        this.main = main;
    }
    public Rain getRain() {
		return rain;
	}

	public void setRain(Rain rain) {
		this.rain = rain;
	}
	
	public Snow getSnow() {
		return snow;
	}

	public void setSnow(Snow snow) {
		this.snow = snow;
	}

    @Override
    public String toString()
    {
        return "[id = "+id+", dt = "+dt+", clouds = "+clouds+", coord = "+coord+", wind = "+wind+", cod = "+cod+", sys = "+sys+", name = "+name+", base = "+base+", weather = "+weather+", main = "+main+"]";
    }
}
	
			
			