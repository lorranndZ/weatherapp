               
public class Sys
{
    private static double message;

    private static int id;

    private static double sunset;

    private static double sunrise;

    private static int type;

    private static String country;

    public double getMessage ()
    {
        return message;
    }

    public void setMessage (double message)
    {
        Sys.message = message;
    }

    public int getId (){
    
        return id;
    }

    public void setId (int id)
    {
        Sys.id = id;
    }

    public double getSunset ()
    {
        return sunset;
    }

    public void setSunset (double sunset)
    {
        Sys.sunset = sunset;
    }

    public double getSunrise ()
    {
        return sunrise;
    }

    public void setSunrise (double sunrise)
    {
        Sys.sunrise = sunrise;
    }

    public int getType ()
    {
        return type;
    }

    public void setType (int type)
    {
        Sys.type = type;
    }

    public String getCountry ()
    {
        return country;
    }

    public void setCountry (String country)
    {
        Sys.country = country;
    }

    @Override
    public String toString()
    {
        return "[message = "+message+", id = "+id+", sunset = "+sunset+", sunrise = "+sunrise+", type = "+type+", country = "+country+"]";
    }
}
			
			