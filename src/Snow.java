import org.codehaus.jackson.annotate.JsonIgnoreProperties;
	
@JsonIgnoreProperties({ "3h","2h","1h" })
public class Snow {
	private static double snow;
	
	public static double getSnow() {
		return snow;
	}

	public static void setSnow(double snow) {
		Snow.snow = snow;
	}


}
