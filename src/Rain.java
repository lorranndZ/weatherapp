import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties({ "3h" , "1h","2h"})

public class Rain {
	private static double rain;
	
	public double getRain(){
		return rain;
	}
	
	public void setRain(double rain) {
		Rain.rain = rain;
	}
	
}
