import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties({ "sea_level", "grnd_level"})
public class Main
{
    private static double humidity;

    private static int pressure;

    private static double temp_max;

    private static double temp_min;

    private static int temp;

    public double getHumidity ()
    {
        return  humidity;
    }

    public void setHumidity (double humidity)
    {
        Main.humidity = humidity;
    }

    public int getPressure ()
    {
        return pressure;
    }

    public void setPressure (int pressure)
    {
        Main.pressure = pressure;
    }

    public double getTemp_max ()
    {
        return temp_max;
    }

    public void setTemp_max (double temp_max)
    {	
        Main.temp_max = temp_max;
    }

    public double getTemp_min ()
    {
        return temp_min;
    }

    public void setTemp_min (double temp_min)
    {
    	
        Main.temp_min = temp_min;
    }

    public int  getTemp ()
    {
        return temp;
    }

    public void setTemp (double temp)
    {   temp = (int) temp - 273.15;
    	Math.round(temp);
        Main.temp = (int) temp;
    }

    @Override
    public String toString()
    {
        return "[humidity = "+humidity+", pressure = "+pressure+", temp_max = "+temp_max+", temp_min = "+temp_min+", temp = "+temp+"]";
    }
}