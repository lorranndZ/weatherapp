import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties({"gust"})
public class Wind {
    private static double speed;

    private  static int deg;

    public double getSpeed ()
    {
        return speed;
    }

    public void setSpeed (double speed)
    {
        Wind.speed = speed;
    }

    public int getDeg ()
    {
        return deg;
    }

    public void setDeg (int deg)
    {
        Wind.deg = deg;
    }

    @Override
    public String toString()
    {
        return "[speed = "+speed+", deg = "+deg+"]";
    }
}
