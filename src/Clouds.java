                 
public class Clouds
{
    private static int all;

    public int getAll ()
    {
        return all;
    }

    public void setAll (int all)
    {
        Clouds.all = all;
    }

    @Override
    public String toString()
    {
        return "[all = "+all+"]";
    }
}
			